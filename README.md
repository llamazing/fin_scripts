# FIN_scripts


## Description
A collection of lua scripts for the FicsIt-Networks mod for the game Satisfactory.

## Usage
TBD. See wiki for detailed instructions and examples.

## Support
For bug reports, feature request and other questions, please open an issue.

## Roadmap
TBD

## License
Licensed as GPLv3
