--[[eeprom.lua	2 Apr 2023	by llamazing
This EEPROM initialization script performs the following tasks:
	* mount the first drive found if possible
	* tries to load require module (first via http then via drive)
	* reads nick settings to configure require module settings
	* runs main.lua via require() as defined by nick settings
	* defines global functions require() and declare()
]]

EEPROM = {_VERSION = '0.1.0'}

--configuration
local HTTP_PATH = "http://localhost:8080/" --http root path, set to false to prevent http loading
local REQUIRE_PATH = "require.lua" --path to require file to load, set to false to skip loading

--convenience
local fs = filesystem
local s_format = string.format

--variables
local inet_card = computer.getPCIDevices(findClass"FINInternetCard")[1]
local is_drive --true if drive was mounted

--// Declares a global variable (useful to indicate global scope is intentional)
function declare(var, value)
	assert(type(var)=="string", "Bad argument #1 to 'declare' (string expected)")
	assert(var:match"^%w[%w_]*$", "Bad argument #1 to 'declare', alpha-numeric & underscore chars only")
	_G[var] = value
end

--// Basic file loader for given http path (string)
	--if loading successful returns return value from loaded file, else retuns nil and error msg
local function load_http(path)
	local result, libdata = inet_card:request(path, "GET", ""):await()
	if not result then return nil, s_format("unable to connect for file %q", path) end

	if result>=200 and result<300 then
		local file_name = path:match"([^/]*)$" --extract file name from end of path
		local chunk, info = load(libdata, file_name)
		if chunk then
			local result = chunk(path) --ignores return values beyond the first one
			return result==nil and true or result --return of nil means load failed so change to true instead
		else return nil, s_format("unable to read file: %s", info) end
	else return nil, s_format("remote file not found: %q", path) end
end


-------------
--  Begin  --
-------------

print(s_format("EEPROM v%s", EEPROM._VERSION))

--try to mount first drive found
if fs.initFileSystem"/dev" then
	local drive
	for _,f in pairs(fs.childs"/dev") do
		if f ~= "serial" then
			drive = f
			break
		end
	end
	if drive then
		fs.mount("/dev/"..drive, "/")
		is_drive = true
		print("drive mounted:", drive)
	else print"no drives found" end
else print"unable to init filesystem" end

if not inet_card then print"no internet card found" end

--try to load require module
if REQUIRE_PATH then
	local is_loaded, info
	print(s_format("loading require module: %q", REQUIRE_PATH))

	--try loading require via internet card if available
	if inet_card and HTTP_PATH then
		local path = HTTP_PATH..REQUIRE_PATH
		print(s_format("loading remote file via http: %q", path))
		is_loaded, info = load_http(path)
		if is_loaded==nil then print(info) end
	end

	--try loading require via filesystem if drive mounted
	if is_loaded==nil and is_drive then
		if fs.exists(REQUIRE_PATH) then is_loaded = fs.doFile(REQUIRE_PATH) end
		if not is_loaded then print(s_format("unable to load file: %q", REQUIRE_PATH)) end
	end

	if is_loaded then
		declare("require", is_loaded)
	else computer.panic"Unable to load require module" end
end

--read nick settings
local settings = {}
do
	local nick = computer.getInstance().nick or ""

	local env = setmetatable({}, {
		--converts variable names to strings so can enter alpha_numeric_underscore strings without quotes
		__index = function(t, k) return k end,
	})
	local code = s_format("return {%s}", nick)
	local is_success, chunk, info = pcall(function() return load(code, nil, 'bt', env) end)
	if not is_success then info = chunk end --so info always contains error msg

	if not is_success or not chunk then
		local error_msg = is_success==nil and chunk or info
		print(s_format("failed to load nick settings: %s", error_msg))
	else settings = chunk() end
end

--apply nick settings
if type(settings.package)=="table" then
	local p = settings.package
	if type(p.path)=="string" then package.path = p.path end
	if type(p.suffix)=="string" then package.suffix = p.suffix end
	if type(p.http)=="string" then package.http = p.http end
end

event.ignoreAll()
event.clear()

--load files specified by settings
if type(settings.require)=="table" then
	if #settings.require>0 and not require then
		computer.panic"require module not available"
	end

	for _,modname in ipairs(settings.require) do
		require(modname)
	end
end
