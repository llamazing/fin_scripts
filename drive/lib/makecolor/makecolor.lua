--[[ makecolor/init.lua
version 0.1
20 Jan 2023
GNU General Public License Version 3
author: Llamazing

   __   __   __   __  _____   _____________  _______
  / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
 / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
/____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/

This module converts colors of various formats to a table array containing 4 RGBA
values in the range 0.0 to 1.0. The alpha value is optional and value of 1.0 will
be used by default.

Usage:
local makecolor = require"makecolor"

--specify color by name with optional alpha value 0-255
color = makecolor"orange"                 --{1.0, 0.647058824, 0, 1.0}
color = makecolor("yellow", 204)          --{1.0, 1.0, 0, 0.8}

--give 3 (or 4) hex literals for r,g,b,a values
color = makecolor(0x00, 0xff, 0x99)       --{0.0, 1.0, 0.6, 1.0}
color = makecolor(0xFF, 0x00, 0x3A, 0x66) --{1.0, 0.0, 0.22745098, 0.2}

--give 3 (or 4) integer values 0-255 for r,g,b,a
color = makecolor(255, 102, 255)          --{1.0, 0.4, 1.0, 1.0}
color = makecolor(0, 255, 0, 153)         --{0.0, 1.0, 0.0, 0.6}

--give a 6 digit hex string (preceding "#" optional)
color = makecolor"#ff002b"                --(1.0, 0.0, 0.168627451, 1.0)
color = makecolor("FFFFFF", 204)           --(1.0, 1.0, 1.0, 0.8)

--give a 3 digit hex string (preceding "#" optional)
color = makecolor"#369"                   --(0.2, 0.4, 0.6, 1.0)
color = makecolor("fff", 0xcc)            --(1.0, 1.0, 1.0, 0.8)

--give table array 2nd optional alpha value 0-255
color = makecolor{0xff, 0xff, 0xff}       --{1.0, 1.0, 1.0, 1.0}
color = makecolor{0, 255, 0, 51}          --{0.0, 1.0, 0.0, 0.2}
color = makecolor({0, 0, 0}, 204)         --(0.0, 0.0, 0.0, 0.8)
color = makecolor({0, 0, 0, 255}, 153)    --(0.0, 0.0, 0.0, 0.6)
]]

local PATH = ...
PATH = PATH:gsub('/init$', '')

local color_list = require(PATH.."/color_list")

--convenience
local sformat = string.format
local mfloor = math.floor

--// Converts a hex color string to an RGB array 0-255
  --hex color (string) - hex color string in the format #FFFFFF or #FFF
    --case-insensitive and # prefix optional
  --returns (table, array) - RBG color (e.g. {255, 255, 255})
local function hex2rgb(hex_color)
  assert(type(hex_color)=="string", "Bad argument #1 to 'hex2rgb', string expected")

  local r,g,b = hex_color:match"#?(%x%x)(%x%x)(%x%x)$"
  if not r then
    r,g,b = hex_color:match"#?(%x)(%x)(%x)$"
    --convert from 3 digit hex color to 6 digit hex color
    r = r*17
    g = g*17
    b = b*17
  end
  assert(r, "Bad argument #1 to 'hex2rgb', string must be 3 or 6 digits")

  return {r,g,b}
end

--// Takes the input args and converts to an RGBA table array, alpha value optional (1.0 default)
--Args can be:
  --A string specifying the color by name (e.g. "darkblue")
  --3 or 4 RGBA 0-255 integers (hex values can be specified using format 0xFF)
  --A table array with 3 or 4 RGBA 0-255 integers
  --3 or 6 character HEX string (leadoing # optional), optional alpha value 0-255 as 2nd parameter
--returns table array with 4 RGBA values 0.0-1.0
local function new_color(...)
  local rgb --table array RGBA 0-255
  local num_args = select("#", ...)
  if num_args==3 or num_args==4 then --arg 4 is optional alpha value 0-255
    rgb = {...}
  elseif num_args==1 or num_args==2 then --arg 2 is optional alpha value 0-255
    local color_value, alpha = ... --arg 1 is table or string, arg 2 is alpha value 0-255
    if type(color_value)=="table" then
      rgb = {}
      for n=1,4 do rgb[n] = color_value[n] end --copy to new table, index 4 optional
    else
      assert(type(color_value)=="string", "Bad argument #1 to 'new_color', string or table expected")
      rgb = color_list[color_value] --lookup color by name

      --must be a hex string
      if not rgb then rgb = hex2rgb(color_value) end
    end

    if alpha then
      alpha = tonumber(alpha)
      assert(alpha, "Bad argument #2 to 'new_color', number value or nil expected")
      alpha = mfloor(alpha)
      assert(alpha>=0 and alpha<=255, "Bad argument #2 to 'new_color', number must be in range 0-255")
      rgb[4] = alpha --overrides existing if present
    end
  end

  --convert RGBA values from 0-255 to 0.0-1.0
  for n,value in ipairs(rgb) do
    local value_type = type(value)
    value = tonumber(value)
    assert(value, sformat("Bad argument #%d to 'new_color', number value expected, got %s", n, value_type))

    value = mfloor(value)
    assert(value>=0 and value <=255, sformat("Bad argument #%d to 'new_color', number value must be from 0-255", n))

    rgb[n] = value/255
  end

  rgb[4] = rgb[4] or 1.0 --use alpha of 1 if not specified
  return rgb
end

--create module object
local meta = {}
local _M = setmetatable({}, meta)
_M.new_color = new_color
_M.hex2rgb = hex2rgb

function meta:__call(...) return new_color(...) end
return _M

--[[ Copyright 2023 Llamazing
  []
  [] This program is free software: you can redistribute it and/or modify it under the
  [] terms of the GNU General Public License as published by the Free Software Foundation,
  [] either version 3 of the License, or (at your option) any later version.
  []
  [] It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [] without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [] PURPOSE.  See the GNU General Public License for more details.
  []
  [] You should have received a copy of the GNU General Public License along with this
  [] program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
