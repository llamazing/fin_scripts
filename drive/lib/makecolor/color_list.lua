--[[ color_list.dat  9 Feb 2019

	from the CSS Color Module Level 4 (Draft, 5 July 2016)
	https://www.w3.org/TR/css-color/#named-colors
	Copyright © 2016 W3C® (MIT, ERCIM, Keio, Beihang). W3C liability, trademark and document use rules apply.

	This data file returns a key/value table with color name strings as keys and the color
	as values expressed as a table array containing 3 numbers 0-255 that correspond to the
	color's red, green & blue components.
]]

return {
	aliceblue = {240, 248, 255},
	antiquewhite = {250, 235, 215},
	aqua = {0, 255, 255},
	aquamarine = {127, 255, 212},
	azure = {240, 255, 255},
	beige = {245, 245, 220},
	bisque = {255, 228, 196},
	black = {0, 0, 0},
	blanchedalmond = {255, 235, 205},
	blue = {0, 0, 255},
	blueviolet = {138, 43, 226},
	brown = {165, 42, 42},
	burlywood = {222, 184, 135},
	cadetblue = {95, 158, 160},
	chartreuse = {127, 255, 0},
	chocolate = {210, 105, 30},
	coral = {255, 127, 80},
	cornflowerblue = {100, 149, 237},
	cornsilk = {255, 248, 220},
	crimson = {220, 20, 60},
	cyan = {0, 255, 255},
	darkblue = {0, 0, 139},
	darkcyan = {0, 139, 139},
	darkgoldenrod = {184, 134, 11},
	darkgray = {169, 169, 169},
	darkgreen = {0, 100, 0},
	darkgrey = {169, 169, 169},
	darkkhaki = {189, 183, 107},
	darkmagenta = {139, 0, 139},
	darkolivegreen = {85, 107, 47},
	darkorange = {255, 140, 0},
	darkorchid = {153, 50, 204},
	darkred = {139, 0, 0},
	darksalmon = {233, 150, 122},
	darkseagreen = {143, 188, 143},
	darkslateblue = {72, 61, 139},
	darkslategray = {47, 79, 79},
	darkslategrey = {47, 79, 79},
	darkturquoise = {0, 206, 209},
	darkviolet = {148, 0, 211},
	deeppink = {255, 20, 147},
	deepskyblue = {0, 191, 255},
	dimgray = {105, 105, 105},
	dimgrey = {105, 105, 105},
	dodgerblue = {30, 144, 255},
	firebrick = {178, 34, 34},
	floralwhite = {255, 250, 240},
	forestgreen = {34, 139, 34},
	fuchsia = {255, 0, 255},
	gainsboro = {220, 220, 220},
	ghostwhite = {248, 248, 255},
	gold = {255, 215, 0},
	goldenrod = {218, 165, 32},
	gray = {128, 128, 128},
	green = {0, 128, 0},
	greenyellow = {173, 255, 47},
	grey = {128, 128, 128},
	honeydew = {240, 255, 240},
	hotpink = {255, 105, 180},
	indianred = {205, 92, 92},
	indigo = {75, 0, 130},
	ivory = {255, 255, 240},
	khaki = {240, 230, 140},
	lavender = {230, 230, 250},
	lavenderblush = {255, 240, 245},
	lawngreen = {124, 252, 0},
	lemonchiffon = {255, 250, 205},
	lightblue = {173, 216, 230},
	lightcoral = {240, 128, 128},
	lightcyan = {224, 255, 255},
	lightgoldenrodyellow = {250, 250, 210},
	lightgray = {211, 211, 211},
	lightgreen = {144, 238, 144},
	lightgrey = {211, 211, 211},
	lightpink = {255, 182, 193},
	lightsalmon = {255, 160, 122},
	lightseagreen = {32, 178, 170},
	lightskyblue = {135, 206, 250},
	lightslategray = {119, 136, 153},
	lightslategrey = {119, 136, 153},
	lightsteelblue = {176, 196, 222},
	lightyellow = {255, 255, 224},
	lime = {0, 255, 0},
	limegreen = {50, 205, 50},
	linen = {250, 240, 230},
	magenta = {255, 0, 255},
	maroon = {128, 0, 0},
	mediumaquamarine = {102, 205, 170},
	mediumblue = {0, 0, 205},
	mediumorchid = {186, 85, 211},
	mediumpurple = {147, 112, 219},
	mediumseagreen = {60, 179, 113},
	mediumslateblue = {123, 104, 238},
	mediumspringgreen = {0, 250, 154},
	mediumturquoise = {72, 209, 204},
	mediumvioletred = {199, 21, 133},
	midnightblue = {25, 25, 112},
	mintcream = {245, 255, 250},
	mistyrose = {255, 228, 225},
	moccasin = {255, 228, 181},
	navajowhite = {255, 222, 173},
	navy = {0, 0, 128},
	oldlace = {253, 245, 230},
	olive = {128, 128, 0},
	olivedrab = {107, 142, 35},
	orange = {255, 165, 0},
	orangered = {255, 69, 0},
	orchid = {218, 112, 214},
	palegoldenrod = {238, 232, 170},
	palegreen = {152, 251, 152},
	paleturquoise = {175, 238, 238},
	palevioletred = {219, 112, 147},
	papayawhip = {255, 239, 213},
	peachpuff = {255, 218, 185},
	peru = {205, 133, 63},
	pink = {255, 192, 203},
	plum = {221, 160, 221},
	powderblue = {176, 224, 230},
	purple = {128, 0, 128},
	rebeccapurple = {102, 51, 153},
	red = {255, 0, 0},
	rosybrown = {188, 143, 143},
	royalblue = {65, 105, 225},
	saddlebrown = {139, 69, 19},
	salmon = {250, 128, 114},
	sandybrown = {244, 164, 96},
	seagreen = {46, 139, 87},
	seashell = {255, 245, 238},
	sienna = {160, 82, 45},
	silver = {192, 192, 192},
	skyblue = {135, 206, 235},
	slateblue = {106, 90, 205},
	slategray = {112, 128, 144},
	slategrey = {112, 128, 144},
	snow = {255, 250, 250},
	springgreen = {0, 255, 127},
	steelblue = {70, 130, 180},
	tan = {210, 180, 140},
	teal = {0, 128, 128},
	thistle = {216, 191, 216},
	tomato = {255, 99, 71},
	turquoise = {64, 224, 208},
	violet = {238, 130, 238},
	wheat = {245, 222, 179},
	white = {255, 255, 255},
	whitesmoke = {245, 245, 245},
	yellow = {255, 255, 0},
	yellowgreen = {154, 205, 50},
}

--[[
	W3C DOCUMENT LICENSE
	Status: This document takes effect 1 February 2015.

	Public documents on the W3C site are provided by the copyright holders under the
	following license.

	License

	By using and/or copying this document, or the W3C document from which this statement
	is linked, you (the licensee) agree that you have read, understood, and will comply
	with the following terms and conditions:

	Permission to copy, and distribute the contents of this document, or the W3C document
	from which this statement is linked, in any medium for any purpose and without fee or
	royalty is hereby granted, provided that you include the following on ALL copies of
	the document, or portions thereof, that you use:
	   * A link or URL to the original W3C document.
	   * The pre-existing copyright notice of the original author, or if it doesn't exist,
	     a notice (hypertext is preferred, but a textual representation is permitted) of
	     the form: "Copyright © [$date-of-document] World Wide Web Consortium, (MIT,
	     ERCIM, Keio, Beihang). http://www.w3.org/Consortium/Legal/2015/doc-license"
	   * If it exists, the STATUS of the W3C document.

	When space permits, inclusion of the full text of this NOTICE should be provided. We
	request that authorship attribution be provided in any software, documents, or other
	items or products that you create pursuant to the implementation of the contents of
	this document, or any portion thereof.

	No right to create modifications or derivatives of W3C documents is granted pursuant
	to this license, except as follows: To facilitate implementation of the technical
	specifications set forth in this document, anyone may prepare and distribute
	derivative works and portions of this document in software, in supporting materials
	accompanying software, and in documentation of software, PROVIDED that all such works
	include the notice below. HOWEVER, the publication of derivative works of this
	document for use as a technical specification is expressly prohibited.

	In addition, "Code Components" —Web IDL in sections clearly marked as Web IDL; and
	W3C-defined markup (HTML, CSS, etc.) and computer programming language code clearly
	marked as code examples— are licensed under the W3C Software License.

	The notice is:
	"Copyright © 2015 W3C® (MIT, ERCIM, Keio, Beihang). This software or document includes
	material copied from or derived from [title and URI of the W3C document]."

	Disclaimers

	THIS DOCUMENT IS PROVIDED "AS IS," AND COPYRIGHT HOLDERS MAKE NO REPRESENTATIONS OR
	WARRANTIES, EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF
	MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, NON-INFRINGEMENT, OR TITLE; THAT
	THE CONTENTS OF THE DOCUMENT ARE SUITABLE FOR ANY PURPOSE; NOR THAT THE IMPLEMENTATION
	OF SUCH CONTENTS WILL NOT INFRINGE ANY THIRD PARTY PATENTS, COPYRIGHTS, TRADEMARKS OR
	OTHER RIGHTS.

	COPYRIGHT HOLDERS WILL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL OR
	CONSEQUENTIAL DAMAGES ARISING OUT OF ANY USE OF THE DOCUMENT OR THE PERFORMANCE OR
	IMPLEMENTATION OF THE CONTENTS THEREOF.

	The name and trademarks of copyright holders may NOT be used in advertising or
	publicity pertaining to this document or its contents without specific, written prior
	permission. Title to copyright in this document will at all times remain with
	copyright holders.

	Notes
	This version: https://www.w3.org/Consortium/Legal/2015/doc-license
	Latest version: https://www.w3.org/Consortium/Legal/copyright-documents
	Previous version: http://www.w3.org/Consortium/Legal/2002/copyright-documents-20021231

	Changes since the previous Document License
	   * This version grants limited permission to create and distribute derivative works
	     except for use as technical specifications.
	   * This version licenses Code Components under the W3C Software License.
]]
