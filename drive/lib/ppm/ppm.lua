--[[ ppm.lua
  31 Mar 2023
  GNU General Public License Version 3
  author: Llamazing

     __   __   __   __  _____   _____________  _______
    / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
   / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
  /____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/

  This module reads PPM image files. Use iter_ppm() for low memory applications.

  PPM file format: https://netpbm.sourceforge.net/doc/ppm.html

  Usage:
    local ppm = require"ppm"
    for n,color1,color2 in ppm.iter_ppm(img_path, optional_file_loader) do
      --where n is the index of the current pixel, starting at 1, only for odd rows
      --color1 is the color of pixel n, array of RGBA values 0-1
      --color2 is the color of the pixel 1 row below pixel n, array of RGBA values 0-1
      --img_path (string) is the path to the ppm image file to use
      --optional_file_loader (function) to specify different loader than io.open
    end
]]

local VERSION = '1.0.0'

--convenience
local s_format = string.format
local t_insert = table.insert

--// Parses PPM header (string) and returns relevant info
  --text (string) - string that begins with the header
    --note: any extra text that follows the header is ignored
  --Returns:
    --id (string) - File format identifier. "P3" & "P6" are valid formats
    --w (number, positive integer) - image width in pixels
    --h (number, positive integer) - image height in pixels
    --maxval (number, positive integer) - The maximum color value, 1 to 65536
      --note: this script currently only supports 255 max
    --header_size (number, positive integer) - The number of bytes in the header
  --returns false if header is invalid or incomplete
local function parse_header(text)
  assert(type(text)=="string", "Bad argument #1 to 'parse_header', string expected")

  local orig_len = text:len() --need to keep track of how many bytes removed

  text = text:gsub("\r\n","\n"):gsub("\r","\n") --normalize line breaks
  text = text:gsub("#.-\n","") --remove comments
  local num_bytes_removed = orig_len - text:len()

  local pattern = "^(%S+%s+%d+%s+%d+%s+%d+%s)"

  local header = text:match"^(%S+%s+%d+%s+%d+%s+%d+%s)"
  if not header then return false end --invalid header format

  local header_size = header:len() + num_bytes_removed
  local id,w,h,maxval = header:match"^(%S+)%s+(%d+)%s+(%d+)%s+(%d+)%s"

  return id, tonumber(w), tonumber(h), tonumber(maxval), header_size
end

--// Reads the header info from the specified file (must already be open)
  --returns
    --id (string) 'P6' for binary format, 'P3' for ASCII plaintext format
    --w (number, positive integer) width of the ppm image in pixels
    --h (number, positive integer) height of the ppm image in pixels
    --maxval (number, positive integer) maximum color value
    --header_size (number, positive integer) size of header in bytes
local function read_header(file)
  local id,w,h,maxval,header_size

  --try reading chunks of 256 bytes at a time until full header captured
  local buf = ""
  local out
  while true do
    out = file:read(256)
    if out then
      buf = buf..out
    else break end

    id,w,h,maxval,header_size = parse_header(buf)
    if id then break end --extracted the full header
  end

  assert(id and (id=="P3" or id=="P6"), s_format("Error in 'read_header': not a valid PPM file"))
  assert(maxval<=255, "Error in 'read_header': only Maxval of 255 or less is supported")

  return id,w,h,maxval,header_size
end


--// Reads a ppm image file from disk and returns its dimensions and header size
  --path (string) - path to ppm file to read
  --file_loader (function, optional) - file loader to read file (default io.open)
local function get_size(path, file_loader)
  file_loader = file_loader or io.open

  assert(type(path)=="string", "Bad argument #1 to 'get_size' (string expected)")
  local file, err = file_loader(path, "r")
  assert(file, s_format("Error in 'get_size': unable to open file. %s", err or ""))

  local id,w,h,maxval,header_size = read_header(file)
  file:close()

  return w, h, header_size
end

--// Reads a PPM image file and returns a table array with RGB values for all pixels
local function read_ppm(path, file_loader)
  file_loader = file_loader or io.open

  assert(type(path)=="string", "Bad argument #1 to 'read_ppm' (string expected)")
  local file, err = file_loader(path, "r")
  assert(file, s_format("Error in 'read_ppm': unable to open file. %s", err or ""))

  local id,w,h,maxval,header_size = read_header(file)

  --read pixel data from PPM file
  local pixels = {w=w,h=h}
  if id=="P6" then --re-open file in binary mode
    file:close()
    file = file_loader(path, "rb")
    file:read(header_size) --skip header
    local w3 = w*3 --row length, used often

    for row=1,h do --each row
      local line = file:read(w3)
      assert(line and line:len()==w3, "Error in 'read_ppm': reached EoF early")

      for n=1,w3,3 do --each pixel
        local r,g,b = line:byte(n,n+2)
        t_insert(pixels, {r/maxval,g/maxval,b/maxval})
      end
    end
  elseif id=="P3" then
    file:seek("set", header_size) --continue file read from end of header

    --read entire file
    local buf = {}
    local out
    while true do
      out = file:read(256)
      if out then
        t_insert(buf, out)
      else
        t_insert(buf, " ") --ensure ends in whitespace
        buf = table.concat(buf)
        break
      end
    end

    local max_size = w*h
    local n=1
    for r,g,b in buf:gmatch"(%d+)%s+(%d+)%s+(%d+)%s+" do
      pixels[n] = {
        tonumber(r)/maxval, tonumber(g)/maxval, tonumber(b)/maxval
      }
      if n==max_size then break end --ignore any extra file data
      n = n + 1
    end
    assert(n==max_size, "Error in 'read_ppm': reached EoF early")
  end
  file:close() --close the file

  return pixels
end


--// Returns iterator to get PPM pixels, one at a time
  --path (string) path to the ppm file
  --file_loader (function, optional) file loader to use, default: io.open
  --(number, optional) alpha value to use for all pixels, 0.0-1.0, default: 1.0
    --use false to omit all alpha values
  --returns iterator function, each call returns:
    --(number) pixel index, starts at 1, left to right then top to bottom, nil when done
    --(table, array) RGB(A) values 0.0 to 1.0 for the current pixel
local function iter_ppm(path, file_loader, alpha)
  file_loader = file_loader or io.open
  if alpha==nil then
    alpha = alpha or 1.0
  elseif alpha==false then
    alpha = nil
  end

  assert(type(path)=="string", "Bad argument #1 to 'iter_ppm' (string expected)")
  local file, err = file_loader(path, "r")
  assert(file, s_format("Error in 'iter_ppm': unable to open file. %s", err or ""))

  assert(not alpha or type(alpha)=="number", "Bad argument #3 to 'iter_ppm', number or nil expected")
  assert(alpha>=0 and alpha<=1.0, "Bad argument #3 to 'iter_ppm', number value must be 0.0-1.0")

  local id,w,h,maxval,header_size = read_header(file) --opens file in read mode
  local max_size = w*h
  local w3 = w*3 --number of bytes per line, used often

  --read pixel data from PPM file
  if id=="P6" then --re-open file in binary mode
    file:close()
    file = file_loader(path, "rb")
    file:seek("set", header_size) --skip header

    local n = 0 --number of pixels read
    local line_index = w3 --start at end of line, will loop back to beginning
    local line = file:read(w3*2)
    return function() --iterator function
      n = n + 1
      if n > max_size then --all pixels read; ignore rest of file
        file:close()
        return
      end

      line_index = line_index + 3
      if line_index>w3 then --end of line reached, pull next lines
        line = file:read(w3) --read 1 line of pixel data
        line_index = 1 --back to start
      end
      if line then
        local r,g,b = line:byte(line_index, line_index+2)
        assert(r, "Error in 'iter_ppm': reached EoF early")
        return n, {r/maxval, g/maxval, b/maxval, alpha}
      end
    end
  elseif id=="P3" then
    file:seek("set", header_size) --continue file read from end of header

    local n = 0 --number of pixels read
    local buf = ""
    return function() --iterator function
      n = n + 1
      if n > max_size then --all pixels read; ignore rest of file
        file:close()
        return
      end

      --first try to grab one pixel from previous buffer
      local r,g,b,remain = buf:match"^%s*(%d+)%s+(%d+)%s+(%d+)%s+(.*)$"
      if not r then --try reading more bytes and add to buffer
        local out = file:read(128)
        if out then
          buf = buff..out
        end
        if n==max_size then buff = buff.." " end --add whitespace to end to ensure search pattern doesn't break

        --try to grab one pixel again
        r,g,b,remain = buf:match"^%s*(%d+)%s+(%d+)%s+(%d+)%s+(.*)$"
        assert(r, "Error in 'iter_ppm': reached EoF early or bad file format")
      end
      buf = remain --remove pixes that were read from buffer

      return n, {
        tonumber(r)/maxval, tonumber(g)/maxval, tonumber(b)/maxval, alpha
      }
    end
  end
end


--// Returns iterator to get PPM pixels, two rows at a time returning 2 pixels
--Note: plaintext PPM not supported
  --path (string) path to the ppm file
  --file_loader (function, optional) file loader to use, default: io.open
  --(number, optional) alpha value to use for all pixels, 0.0-1.0, default: 1.0
    --use false to omit all alpha values
  --returns iterator function, each call returns:
    --(number) pixel index, starts at 1, left to right then top to bottom
      --skips even rows, is nil when done
    --(table, array) RGB(A) values 0.0 to 1.0 for the current pixel
    --(table, array) RGB(A) values 0.0 to 1.0 for the pixel 1 row below
local function iter_ppm_2rows(path, file_loader, alpha)
  file_loader = file_loader or io.open
  if alpha==nil then
    alpha = 1.0
  elseif alpha==false then
    alpha = nil
  end

  assert(type(path)=="string", "Bad argument #1 to 'iter_ppm' (string expected)")
  local file, err = file_loader(path, "r")
  assert(file, s_format("Error in 'iter_ppm': unable to open file. %s", err or ""))

  assert(not alpha or type(alpha)=="number", "Bad argument #3 to 'iter_ppm', number or nil expected")
  assert(alpha>=0 and alpha<=1.0, "Bad argument #3 to 'iter_ppm', number value must be 0.0-1.0")

  local id,w,h,maxval,header_size = read_header(file) --opens file in read mode
  local max_size = w*h
  local w3 = w*3 --number of bytes per line, used often

  --read pixel data from PPM file
  assert(id=="P6", "Error in iter_ppm_2rows', plaintext P3 PPM format not supported")
  file:close()
  file = file_loader(path, "rb")
  file:seek("set", header_size) --skip header

  local n = 0 --number of pixels read
  local line_index = w3 --start at end of line, will loop back to beginning
  local line = file:read(w3*2)
  return function() --iterator function
    n = n + 1
    if n > max_size then --all pixels read; ignore rest of file
      file:close()
      return
    end

    line_index = line_index + 3
    if line_index>w3 then --end of line reached, pull next lines
      line = file:read(w3*2) --read 2 lines of pixel data
      line_index = 1 --back to start
      n = n + w --skip a line of pixels already read
    end
    if line then
      local r,g,b = line:byte(line_index, line_index+2)
      local r2,g2,b2 = line:byte(line_index+w3,line_index+w3+2)
      assert(r, "Error in 'iter_ppm': reached EoF early")
      return n,
        {r/maxval, g/maxval, b/maxval, alpha},
        r2 and {r2/maxval, g2/maxval, b2/maxval, alpha} --nil if last line and odd number of rows
    end
  end
end


return {
  _VERSION = VERSION,
  read_ppm = read_ppm,
  get_size = get_size,
  iter_ppm = iter_ppm,
  iter_ppm_2rows = iter_ppm_2rows,
}


--[[ Copyright 2023 Llamazing
  []
  [] This program is free software: you can redistribute it and/or modify it under the
  [] terms of the GNU General Public License as published by the Free Software Foundation,
  [] either version 3 of the License, or (at your option) any later version.
  []
  [] It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [] without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [] PURPOSE.  See the GNU General Public License for more details.
  []
  [] You should have received a copy of the GNU General Public License along with this
  [] program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
