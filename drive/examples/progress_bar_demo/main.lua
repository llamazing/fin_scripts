--[[progress_bar_demo/main.lua
version 1.0.0
13 Mar 2023
GNU General Public License Version 3
author: Llamazing

   __   __   __   __  _____   _____________  _______
  / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
 / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
/____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/

This script manages a gpu and screen with touch-screen interface to showcase the
usage of three various styles of progress bars.
]]

local PATH = ... or ""
local main_menu = require(PATH.."main_menu")
local pb_menu = require(PATH.."progress_bar_menu")

--convenience
local t_remove = table.remove
local m_min = math.min
local m_random = math.random


------------------
-- INITIAL SETUP --
-------------------

math.randomseed(computer.magicTime())

--get first T1 GPU avialable from PCI-Interface
local gpu = computer.getPCIDevices(findClass"GPUT1")[1]
assert(gpu, "No GPU T1 found!")

-- get first Screen-Driver available from PCI-Interface
local screen = computer.getPCIDevices(findClass"FINComputerScreen")[1]
-- if no scren found,w try to find large screen from component network
if not screen then
 local comp = component.findComponent(findClass"Screen")[1]
 assert(comp, "No Screen found!")
 screen = component.proxy(comp)
end

--list of available menus, menu keys should be lowercase
local MENUS = {
  main = main_menu.new(),
  high_res = pb_menu.new{
    type = "high_res",
    width = 18,
  },
  pretty = pb_menu.new{
    type = "pretty",
    width = 18,
  },
  spinner = pb_menu.new{
    type = "spinner",
  },
}
for _,menu in pairs(MENUS) do menu:set_gpu(gpu) end

--(table) array of menus that are active, the highest index is on top
--menus in the array are also stored as keys to check if present
local active_menus = {}

local active_menu = nil --(table) the menu that is active and visible
local percent = nil --(number, percent) progress of the active progress bar, 0.0-1.0
  --nil if active menu does not have progress bar or if progress bar is spinner

--// Activates the given menu, putting it on the top of the stack (in front)
--NOTE: the same instance of a given menu cannot be started if already active
  --menu (table) - menu to make active
local function start_menu(menu)
  if type(menu)=="string" then
    local menu_name = menu
    menu = MENUS[menu]
    assert(menu, "Bad argument #1 to 'start_menu', invalid menu name: "..menu_name)
  end
  assert(type(menu)=="table", "Bad argument #1 to 'start_menu' (table or string expected")

  --start the new menu
  assert(not active_menus[menu], "Error in 'start_menu', menu is already active")
  table.insert(active_menus, menu)
  active_menus[menu] = true
  active_menu = menu

  --initialize menu and draw to screen
  if menu.OnStarted then menu:OnStarted() end
  gpu:flush()

  --update percent value for newly active menu
  percent = menu.get_value and menu:get_value()
  if type(percent)~="number" then percent = nil end

  return menu
end

--// Closes top menu and re-activates the previous menu on the top of the stack
  --returns true if a menu was closed, else returns false
local function close_menu()
  local top_menu = t_remove(active_menus, #active_menus) --TODO error if empty? double-check
  if top_menu then
    --close the active menu
    active_menus[top_menu] = nil
    if top_menu.OnFinished then top_menu:OnFinished() end

    --re-open new top-most menu, if applicable
    active_menu = active_menus[#active_menus]
    if active_menu and active_menu.OnStarted then active_menu:OnStarted() end

    return true --success
  end

  return false --no active menu to close
end

--// Runs the given action command (string)
--valid actions:
  --"close_menu" - closes top menu using close_menu()
  --"open:<menu_name>" - opens menu specified by name (case-insensitive)
local function run_action(action)
  assert(type(action)=="string", "Bad argument #1 to 'run_action' (string expected)")

  if action=="close_menu" then
    close_menu()
  else
    local command, parameter = action:match"^(%w[%w_]*)%:(%w[%w_]*)$"
    if command then
      command = command:lower()
      parameter = parameter:lower()

      if command=="open" then start_menu(parameter) end
    end
  end
end


---------------
-- MAIN LOOP --
---------------

-- setup gpu
event.listen(gpu)
gpu:bindScreen(screen)

start_menu(MENUS.main)

while true do
  e, s, x, y, button = event.pull(0.06)
  if e == "OnMouseDown" then
    if active_menu.OnMouseDown then
      local is_update = active_menu:OnMouseDown(x, y, button)
      if is_update then gpu:flush() end
    end
  elseif e == "OnMouseUp" then
    if active_menu.OnMouseUp then
      local action = active_menu:OnMouseUp(x, y, button)
      gpu:flush() --assume always needs refresh on mouse up
      if action then run_action(action) end
    end
  elseif not e then --on timeout update percent
    --advance progress bar by random amount
    local amt = active_menu==MENUS.pretty and 0.03 or 0.018
    if percent and percent < 1 then
      percent = m_min(percent + m_random()*amt+amt/2, 1)
    end

    if active_menu.update then
      active_menu:update(percent) --NOTE: percent is nil for spinner menu
    end
    gpu:flush()
  end
end


--[[ Copyright 2023 Llamazing
  []
  [] This program is free software: you can redistribute it and/or modify it under the
  [] terms of the GNU General Public License as published by the Free Software Foundation,
  [] either version 3 of the License, or (at your option) any later version.
  []
  [] It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [] without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [] PURPOSE.  See the GNU General Public License for more details.
  []
  [] You should have received a copy of the GNU General Public License along with this
  [] program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
