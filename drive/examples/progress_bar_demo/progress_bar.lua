--[[progress_bar.lua
25 Feb 2023
GNU General Public License Version 3
author: Llamazing

   __   __   __   __  _____   _____________  _______
  / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
 / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
/____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/

This script manages the state of various implementations of progress bars, which
when updated return a fixed width string showing how the progress bar appears as
text.

Progress bar styles available:
"pretty" - draws a box around the progress bar, fills left-to-right
"high_res" - left-to-right progress bar with 1/8th segments for each character
"spinner" - single width character that has spinning animation while active

Usage:
local progress_bar = require"progress_bar"

local pb = progress_bar.new{ --creates new progress bar instance
	type = "high_res", --which style of progress bar to use
	width = 12, --width of the progress bar in number of characters
}
local pb_text = pb:set(0.25) --generates text for 25% full progress bar

local spinner = progress_bar.new("spinner") --short hand if no other properties
local spinner_text = spinner:set(true) --start spinning animation
spinner_text = spinner:update() --call each frame to update animation
]]


local meta = {}
local _M = setmetatable({
	VERSION = '1.0.0'
},meta)

--convenience
local mfloor = math.floor
local mmax = math.max
local sformat = string.format
local srep = string.rep

--key/value table to lookup unicode chars used by progress bars
local BAR_CHARS = {
	left = {
		[false] = '\u{ee00}', --left-most progress bar segment, empty
		[true] = '\u{ee03}', --left-most progress bar segment, full
	},
	middle = {
		[false] = '\u{ee01}', --middle progress bar segment, empty
		[true] = '\u{ee04}', --middle progress bar segment, full
	},
	right = {
		[false] = '\u{ee02}', --right-most progress bar segment, empty
		[true] = '\u{ee05}', --right-most progress bar segment, full
	},
	horz = {
		[0] = ' ', --blank (space char)
		'\u{258f}', --1/8 left block
		'\u{258e}', --1/4 left block
		'\u{258d}', --3/8 left block
		'\u{258c}', --1/2 left block
		'\u{258b}', --5/4 left block
		'\u{258a}', --3/4 left block
		'\u{2589}', --7/8 left block
		'\u{2588}', --full block
	},
	spinner = {
		[0] = ' ', --blank (space char)
		--These spinning progress bar characters are unique to the FiraCode font
		'\u{ee06}',
		'\u{ee07}',
		'\u{ee08}',
		'\u{ee09}',
		'\u{ee0a}',
		'\u{ee0b}',
	},
}
local SPINNER_MAX_COUNT = #BAR_CHARS.spinner

--Implementation of :new() method depends on progress bar type
local CONSTRUCTORS = {
	--A progress bar outlined in a box
	pretty = function(properties)
		local progress_bar = {}

		--private variables
		local display_string = nil --(string) text of how progress bar appears
		local value = nil --(number) percent progress bar is full (0-1); default is 0
		local width = nil --(number) how many characters wide the progress bar is (min 2); default is 10

		--validate properties (if applicable)
		local percent = tonumber(properties.percent or 0)
		assert(percent, "Bad property 'percent' to 'new' (number or nil expected)")
		assert(percent>=0 and percent <=1, "Bad property 'percent' to 'new', number value must be in range 0-1")
		local width = tonumber(properties.width or 10)
		assert(width, "Bad property 'width' to 'new' (number or nil expected)")
		width = mfloor(width)
		assert(width>=2, "Bad property 'width' to 'new', number value must be greater than or equal to 2")


		--------------------
		-- Member Methods --
		--------------------

		--// Returns the string of how the progress bar currently appears
		function progress_bar:get_string() return display_string end
		--// Returns number of glyphs in progress bar (not affected by percent full)
  		function progress_bar:get_width() return width end
		--// Returns percent (number from 0 to 1) representing progress bar's current value
		function progress_bar:get() return value end
		--// Sets the progress bar to the given percent (number from 0 to 1)
			--returns (string) of how progress bar appears
		function progress_bar:set(percent)
			assert(type(percent)=="number", "Bad argument #1 to 'set' (number expected)")
			assert(percent>=0 and percent<=1, "Bad argument #1 to 'set', number must be in range 0-1")

			local bar_length = mfloor(width*percent + 0.5) --number of characters full progress bar is (round nearest)

			local bar_start = BAR_CHARS.left[bar_length>0] --unicode char to use for left edge of progress bar
			local bar_end = BAR_CHARS.right[bar_length==width] --unicode char to use for right edge of progress bar
			local full_seg_count = mmax(0, bar_length - 1 - (bar_length==width and 1 or 0)) --how many middle segments are full
			local empty_seg_count = width - 2 - full_seg_count --how many middle segments are empty

			--concatenate chars together and return string representing progress bar in current state
			display_string = sformat("%s%s%s%s",
				bar_start,
				BAR_CHARS.middle[true]:rep(full_seg_count),
				BAR_CHARS.middle[false]:rep(empty_seg_count),
				bar_end
			)
			value = percent

			return display_string
		end

		display_string = progress_bar:set(percent) --set initial text to display

		return progress_bar
	end,

	--A high-resolution progress bar with no frills
	high_res = function(properties)
		local progress_bar = {}

		--private variables
		local display_string = nil --(string) text of how progress bar appears
		local value = nil --(number) percent progress bar is full (0-1); default is 0
		local width = nil --(number) how many characters wide the progress bar is (min 2); default is 10

		--validate properties (if applicable)
		local percent = tonumber(properties.percent or 0)
		assert(percent, "Bad property 'percent' to 'new' (number or nil expected)")
		assert(percent>=0 and percent <=1, "Bad property 'percent' to 'new', number value must be in range 0-1")
		local width = tonumber(properties.width or 10)
		assert(width, "Bad property 'width' to 'new' (number or nil expected)")
		width = mfloor(width)
		assert(width>=2, "Bad property 'width' to 'new', number value must be greater than or equal to 2")


		--------------------
		-- Member Methods --
		--------------------

		--// Returns the string of how the progress bar currently appears
		function progress_bar:get_string() return display_string end
		--// Returns number of glyphs in progress bar (not affected by percent full)
		function progress_bar:get_width() return width end

		--// Returns percent (number from 0 to 1) representing progress bar's current value
		function progress_bar:get() return value end
		--// Sets the progress bar to the given percent (number from 0 to 1)
			--returns (string) of how progress bar appears
		function progress_bar:set(percent)
			assert(type(percent)=="number", "Bad argument #1 to 'set' (number expected)")
			assert(percent>=0 and percent<=1, "Bad argument #1 to 'set', number must be in range 0-1")

			local bar_length = mfloor(width*8*percent + 0.5) --Number of 'bits' full progress bar is (each char is subdivided into eights, round nearest)

			local full_char = BAR_CHARS.horz[8] --unicode char to use for full character
			local full_char_count = bar_length // 8 --how many characters are completely full
			local partial_char = BAR_CHARS.horz[bar_length % 8] --unicode char to represent transition character (from blank to 7/8 full)
			local blank_char = BAR_CHARS.horz[0] --unicode char to use for blank characters
			local blank_count = width - full_char_count - 1 --how many characters are blank (tentative)
			if full_char_count==width then --special case if bar is completely full
				partial_char = '' --when bar is 100% full, don't want to show partial character at all
				blank_count = 0
			end

			display_string = sformat("%s%s%s",
				full_char:rep(full_char_count),
				partial_char,
				blank_char:rep(blank_count)
			)
			value = percent

			return display_string
		end

		display_string = progress_bar:set(percent) --set initial text to display

		return progress_bar
	end,

	--The spinner progress bar is a single character that displays a spinning animation
		--call :start() or :set(true) to begin and :stop() or :set(false) to display blank char
		--call :update() every time the spinner should advance 1 frame and it returns a string as it appears
	spinner = function(properties)
		local progress_bar = {}

		--private variables
		local display_string = nil --(string) text of how progress bar appears
		local value = properties.value~=false and 1 or nil --(number) current frame index of animation (starts at 1)
		--nil means inactive, 0 is not visible but will advance on next :update()


		--------------------
		-- Member Methods --
		--------------------

		--// Returns the string of how the progress bar currently appears
		function progress_bar:get_string() return display_string end
		--// Returns number of glyphs in progress bar
		function progress_bar:get_width() return 1 end

		--// Starts progress bar spinning
			--returns (string) single unicode character of how progress bar appears
			--equivalent to: progress_bar:set(true)
		function progress_bar:start()
			value = 0
			return self:update()
		end

		--// Stops progress bar spinning and shows as blank space
			--returns (string) single unicode character of how progress bar appears
			--equivalent to progress_bar:set()
		function progress_bar:stop()
			value = nil
			return self:update()
		end

		--// Returns true if progress bar is currently spinning, else false
		function progress_bar:get() return not not value end
		--// If bool is true then starts spinner, else if false stops spinner
			--returns (string) single unicode character of how progress bar appears
		function progress_bar:set(bool)
			assert(type(bool)=="boolean", "Bad argument #1 to 'set' (boolean expected)")

			value = bool and 0 or nil
			return self:update()
		end

		--// Causes progress bar to advance spinning by 1 step
			--returns (string) single unicode character of how progress bar appears
		function progress_bar:update()
			if value then
				value = value + 1
				if value > SPINNER_MAX_COUNT then value = 1 end
			end

			display_string = BAR_CHARS.spinner[value or 0]

			return display_string
		end

		display_string = progress_bar:set(not not value) --set initial text to display

		return progress_bar
	end,
}

--// Creates and returns a new progress bar instance with the given properties
	--properties (table, key/value) - configures the progress bar
		--type (string, optional) - defines the style of progress bar to use:
			--"high_res" - progress bar with 8 segments per width character (default)
			--"pretty" - looks fancy with boxed outline, 1 segment per width character
			--"spinner" - spins continuously until stopped, width of 1 character
		--width (number, positive integer, optional) - how many characters wide
			--not applicable to spinner, default 10
		--value (number, non-negative, optional) - starting value of progress bar
			--progress bars: 0.0 for empty, 1.0 for full (default: 0.0)
			--spinner: false is inactive, else index (1+) of starting frame (default: 1)
		--returns (table) - newly created progress bar instance
function _M.new(properties)
	if type(properties)=="string" then properties = {type=properties} end --convert to table
	if not properties then properties = {type="high_res"} end --default
	assert(type(properties)=="table", "Bad argument #1 to 'new' (table or string or nil expected)")
	assert(type(properties.type)=="string", "Bad property 'type' to 'new' (string expected)")
	local constructor = CONSTRUCTORS[properties.type]
	assert(constructor, "Bad property 'type' to 'new', invalid progress bar type: "..properties.type)

	return constructor(properties)
end

function meta:__call(...) return self.new(...) end

return _M


--[[ Copyright 2023 Llamazing
  []
  [] This program is free software: you can redistribute it and/or modify it under the
  [] terms of the GNU General Public License as published by the Free Software Foundation,
  [] either version 3 of the License, or (at your option) any later version.
  []
  [] It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [] without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [] PURPOSE.  See the GNU General Public License for more details.
  []
  [] You should have received a copy of the GNU General Public License along with this
  [] program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
