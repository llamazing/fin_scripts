--[[main_menu.lua
13 Mar 2023
GNU General Public License Version 3
author: Llamazing

   __   __   __   __  _____   _____________  _______
  / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
 / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
/____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/

Displays a touch-screen menu to select between 3 different progress bar menus.
]]


local meta = {}
local _M = setmetatable({
  VERSION = '1.0.0'
},meta)

--convenience
local t_unpack = table.unpack or unpack

local WIDTH, HEIGHT = 28, 7 --desired screen size
local BG_COLOR = {0, 0, 0, 1}
local FG_COLOR = {1,1,1,1}
local HIGHLIGHT_COLOR = {0.1, 0.25, 0.5, 1} --color when button pressed

--unicode chars to draw boxes around buttons
local BOX_CHARS = {
  UL = '\u{250c}',
  UR = '\u{2510}',
  LL = '\u{2514}',
  LR = '\u{2518}',
  H = '\u{2500}',
  V = '\u{2502}',
}

--list of buttons used by this menu
local BUTTONS = {
  {
    text = "Pretty",
    action = "open:pretty",
  },
  {
    text = "High-Res",
    action = "open:high_res",
  },
  {
    text = "Spinner",
    action = "open:spinner",
  },
}

--// Creates and returns a new menu instance with given properties (table)
function _M.new(properties)
  local menu = {}

  local gpu = nil
  local menu_width, menu_height --actual screen size
  local highlight_index = nil --index of button currently highlighted or nil if none

  --// clears entire screen
  local function clear_screen()
    gpu:setBackground(t_unpack(BG_COLOR))
    gpu:setForeground(t_unpack(FG_COLOR))
    gpu:fill(0, 0, menu_width, menu_height, " ")
  end

  --// Draws a button at given x/y coordinates with size depending on text length
  local function draw_button(x, y, text)
    local width = text:len()
    gpu:setText(x, y, BOX_CHARS.UL..BOX_CHARS.H:rep(width)..BOX_CHARS.UR)
    gpu:setText(x, y+1, BOX_CHARS.V..text..BOX_CHARS.V)
    gpu:setText(x, y+2, BOX_CHARS.LL..BOX_CHARS.H:rep(width)..BOX_CHARS.LR)
  end

  --// draws entire screen from scratch (call clear_screen() first)
  local function draw_screen()
    gpu:setText(3, 1, "Select a progress bar:")
    local x_offset = 0
    gpu:flush()

    for i,info in ipairs(BUTTONS) do
      --save info about button size/location for mouse events
      local info = BUTTONS[i]
      info.x = x_offset
      info.y = 3
      info.width = info.text:len()+2
      info.height = 3

      draw_button(x_offset, 3, info.text)
      x_offset = x_offset + info.width
    end
  end

  --// Highlights the specified button by index, changing the fg color
  --also used to restore button to original color (unhighlight)
    --i (number) index of button to highlight (starts at 1)
    --color (table, array) - 4 numbers 0-1 for RGBA values of button fg color
  local function highlight_button(i, color)
    local info = BUTTONS[i]
    gpu:setForeground(t_unpack(color))
    draw_button(info.x, info.y, info.text) --text didn't change but have to re-write it to change fg color
  end

  --// Returns index of button at given x/y coordinates or nil if none
  local function find_button(x, y)
    for i,info in ipairs(BUTTONS) do
      local x_max = info.x + info.width - 1
      local y_max = info.y + info.height - 1
      if x>=info.x and y>=info.y and x<=x_max and y<=y_max then
        return i
      end
    end

    return nil
  end

  --// Assigns the gpu used by the menu, must be set before starting menu
  function menu:set_gpu(new_gpu) gpu = new_gpu end

  --// To be called when the menu is activated, initializes menu
  function menu:OnStarted()
    assert(gpu, "Error: No GPU assigned, use menu:set_gpu() to assign a GPU")

    --reset menu state
    highlight_index = nil

    --set gpu to size of new menu
    gpu:setSize(WIDTH, HEIGHT)
    menu_width, menu_height = gpu:getSize()

    --draw initial screen
    clear_screen()
    draw_screen()
  end

  --// To be called when the menu is closed
  function menu:OnFinished()
    highlight_index = nil
  end

  --// To be called when mouse button is pressed down, highlights button at cursor
  function menu:OnMouseDown(x, y, button)
    local i = find_button(x, y)
    if i then
      --remove previous highlight
      if highlight_index and highlight_index ~= i then
        highlight_button(highlight_index, FG_COLOR)
      end

      --highlight button at cursor
      highlight_button(i, HIGHLIGHT_COLOR)
      highlight_index = i

      return true
    end
  end

  --// To be called when mouse button is released, activates clicked button
  function menu:OnMouseUp(x, y, button)
    local i = find_button(x, y)
    local action
    if i and highlight_index == i then
      action = BUTTONS[i].action or true
    end

    --remove existing highlight
    if highlight_index then
      highlight_button(highlight_index, FG_COLOR)
      highlight_index = nil
    end

    return action
  end

  --// To be called when mouse cursor moves
  function menu:OnMouseMoved(x, y, button)
    --UNUSED
  end

  return menu
end

function meta:__call(...) return self.new(...) end

return _M


--[[ Copyright 2023 Llamazing
  []
  [] This program is free software: you can redistribute it and/or modify it under the
  [] terms of the GNU General Public License as published by the Free Software Foundation,
  [] either version 3 of the License, or (at your option) any later version.
  []
  [] It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [] without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [] PURPOSE.  See the GNU General Public License for more details.
  []
  [] You should have received a copy of the GNU General Public License along with this
  [] program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
