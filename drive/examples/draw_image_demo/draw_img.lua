--[[ draw_img.lua
  version 1.0.0
  31 Mar 2023
  GNU General Public License Version 3
  author: Llamazing

     __   __   __   __  _____   _____________  _______
    / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
   / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
  /____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/

  This script reads a PPM image file stored on a drive and displays it on a screen.
]]

local ppm = require"ppm"

--convenience
local fs = filesystem
local m_min = math.min

local IMG_PATH = "img/jace.ppm"
local EMPTY_COLOR = {0, 0, 0, 0} --color of bottom row if image height is not even

-- get first T1 GPU avialable from PCI-Interface
local gpu = computer.getPCIDevices(findClass("GPUT1"))[1]
assert(gpu, "No GPU T1 found!")

-- get first Screen-Driver available from PCI-Interface
local screen
--local screen = computer.getPCIDevices(findClass("FINComputerScreen"))[1]
-- if no scren found, try to find large screen from component network
--if not screen then
 local comp = component.findComponent(findClass("Screen"))[1]
 assert(comp, "No Screen found!")
 screen = component.proxy(comp)
--end

--read ppm image file header to get image dimensions
local img_w,img_h = ppm.get_size(IMG_PATH, fs.open)
local img_2w = 2*img_w

-- setup gpu
gpu:setSize(img_w,(img_h+1)//2)
gpu:bindScreen(screen)
local w,h = gpu:getSize()
assert((img_w<=w) and (img_h<=(2*h)), "Error image is too big, max size: 300x200")

-- clean screen
local function clear_screen()
  gpu:setBackground(0, 0, 0, 0)
  gpu:fill(0, 0, w, h, " ")
end
clear_screen()
gpu:flush()

--draw image on screen
local buffer = gpu:getBuffer()
for n,color_fg,color_bg in ppm.iter_ppm_2rows(IMG_PATH, fs.open) do --n starts at 1
  local x = (n-1)%img_w --x starts at 0
  local y = (n-1)//img_2w --y starts at 0

  buffer:set(x, y, "\u{2580}", color_fg, color_bg)
end

gpu:setBuffer(buffer)
gpu:flush()

--[[ Copyright 2023 Llamazing
  []
  [] This program is free software: you can redistribute it and/or modify it under the
  [] terms of the GNU General Public License as published by the Free Software Foundation,
  [] either version 3 of the License, or (at your option) any later version.
  []
  [] It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [] without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [] PURPOSE.  See the GNU General Public License for more details.
  []
  [] You should have received a copy of the GNU General Public License along with this
  [] program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
