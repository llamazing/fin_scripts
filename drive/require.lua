--[[require.lua
  31 Mar 2023
  GNU General Public License Version 3
  author: Llamazing

     __   __   __   __  _____   _____________  _______
    / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
   / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
  /____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/

  This is a pure lua implementation of require with the following deviations:
	* Differing mod names resulting in the same absoulte path are only loaded once
  * Passing a url string that begins with 'http://' will load from the internet
		- package.http (string) lists http paths to search (separate by semicolons)
		- setting package.http to false disables all http loading
  * The path passed to a script on loading is the absolute directory path
  * Uses the FIN filesystem API to load scripts (can only load lua scripts)
]]

local meta = {}
local _M = setmetatable({
	_VERSION = '1.0.0',
}, meta)

package = package or {}
package.path = "/?;/lib/?" --(string) list of paths to try loading up to end of modname, separate with semi-colons; should not end in "/"; suffixes added separately
package.suffix = ".lua;/init.lua;/?.lua" --non-standard, (string) list of suffixes to append to paths, separate with semi-colons; "?" substitutes for stub at end of path
package.http = "http://localhost:8080/" --non-standard, (string) list of base http urls to try, separate with semi-colons; should end with "/"
package.preload = package.preload or {} --(table, key/value) list of pre-loaded chunks to try to load first before looking for file to read
package.loaded = package.loaded or {} --(table, key/value) cached return values from initial loading, keys are absolute path to loaded file
package.modnames = {} --non-standard, (table, key/value) associates modname (keys as string) with full_path (values as string) that was used for initial load

--convenience
local fs = filesystem
local p_path = package.path
local p_suffix = package.suffix
local p_http = package.http
local p_preload = package.preload
local p_loaded = package.loaded
local p_modnames = package.modnames
local s_format = string.format
local s_gmatch = string.gmatch
local t_concat = table.concat


local function normalize_path(path)
	local path_orig = path
	path = path:gsub("^//+", "/") --consolidate multiple slashes at start
	path = path:gsub("([^:/])//+", "%1/") --consolidate multiple slashes to single slash (except for '://' like in http path)
	path = path:gsub("[^/]+/[^/]*%.%./", "") -- './' means current directory
	path = path:gsub("[^/]*%./", "") --'../' means upper directory
	return path
end

--// returns (table, array) all combinations of paths and suffixes for the given modname (string)
local function get_paths(modname)
	local path_list = {normalize_path("/"..modname)}
	for path in s_gmatch(p_path, "[^;]+") do
		for suffix in s_gmatch(p_suffix, "[^;]+") do
			local dir = path:gsub('%?', modname)
			local stub = dir:match".+/([^/]+)" or dir --everything past final "/"
			suffix = suffix:gsub('%?', stub)
			table.insert(path_list, normalize_path(dir..suffix))
		end
	end
	return path_list
end

--// Given path (string) returns text following final instance of "/", giving file name + extension (if applicable)
	--returns (string) - file name at end of path
local function get_file_name(path)
	return path:match"^.*/([^/]+)$" or path --if match not found then path does not contain "/"
end

--// List of loader functions to try in sequence
	--modname (string) name of the module to load
	--returns:
		--(non-nil) value returned by loaded module; nil if loading failed
		--(string) absolute path to loaded module; error message if loading failed
local LOADERS = {
	--returns previously loaded data if it exists instead of loading again
	function(modname, _)
		local full_path = p_modnames[modname]
		return full_path and p_loaded[full_path]
	end,
	--preloader
	--looks for preloader function to use
	function(modname, paths)
		local msgs = {}
		for i,path in ipairs(paths) do
			local preload = p_preload[path]
			if preload~=nil then
				return preload, path
			end
			msgs[#msgs+1] = s_format("\nno field package.preload[%q]", path)
		end
		return nil, t_concat(msgs, '') --no path with preload data found
	end,
	--file loader
	--loads lua script from filesystem
	function(modname, paths)
		local msgs = {}
		for i,full_path in ipairs(paths) do
			if fs.isFile(full_path) then
				if p_loaded[full_path]~=nil then --was loaded previously as different name
					p_modnames[modname] = full_path
					return p_loaded[full_path]
				end
				return nil, fs.loadFile(full_path), full_path
			else msgs[#msgs+1] = s_format("\nno file %q", full_path) end
		end
		--not successful, no file found
		return nil, t_concat(msgs, '')
	end,
	--url loader
	--loads lua script from http request over internet
	function(modname, _)
		if p_http==false then return nil, "\nhttp loading disabled" end
		local card = computer.getPCIDevices(findClass"FINInternetCard")[1]
		if not card then return nil, "\nno internet card installed" end
		local msgs = {}

		--tries to load the exact path given via http
		local function fetch_http(path)
			if p_loaded[path] ~= nil then --was loaded previously as different name
				p_modnames[modname] = path
				return p_loaded[path]
			end
			local result, libdata = card:request(path, "GET", ""):await()
			if result then
				if result<200 or result>=300 then
					msgs[#msgs+1] = s_format("\nno remote file %q", path)
				else return nil, libdata, path end
			else msgs[#msg+1] = s_format("\nunable to connect for file %q", path) end
		end

		--appends suffix variations to http path and tries to load via http
		local function load_http(modname)
			if modname:match("%.lua$") then --includes file extension, try loading
				return fetch_http(modname)
			else --try appending all suffixes, assume all end in .lua
				for suffix in s_gmatch(p_suffix, "[^;]+") do
					local stub = modname:match"^.+/([^/]+)$" or dir --everything past final "/"
					suffix = suffix:gsub('%?', stub)
					local result, libdata, full_path = fetch_http(modname..suffix)
					if result~=nil or libdata then return result, libdata, full_path end
				end
			end
		end

		--converts modname to http path and tries to load
		local function load_http_with_prefix(modname)
			if modname:match"^https?://" then --is full http url, try loading it
				return load_http(modname)
			else --prepend http roots
				for http_prefix in s_gmatch(p_http, "[^;]+") do
					if http_prefix:match"[^/]$" then http_prefix = http_prefix.."/" end --ensure ends in "/"
					for path in s_gmatch(p_path, "[^;]+") do
						local full_path = normalize_path(http_prefix..path:gsub('%?', modname))
						local result, libdata, full_path = load_http(full_path)
						if result~=nil or libdata then return result, libdata, full_path end
					end
				end
			end
		end

		local result, libdata, full_path = load_http_with_prefix(modname)
		if result~= nil then --previously loaded, this is the result
			return result
		elseif libdata then --loading successful
			--TODO save file to filesystem at specified modname location if specified by settings
			local file_name = get_file_name(full_path) --used in error messages
			return nil, load(libdata, file_name), full_path
		else return nil, t_concat(msgs, '') end
	end,
}
package.searchers = LOADERS

local function search_all(modname)
	local msgs = {s_format("module %q not found:", modname)} --keep list of all failed loading attempts; tentative
	local paths = get_paths(modname) --(table, array) list of paths to try to load
	local result, info, full_path
	for _,searcher in ipairs(LOADERS) do
		result, info, full_path = searcher(modname, paths)
		if result~=nil then
			return result
		elseif type(info)=="function" then
			local chunk = info
			return nil, chunk, full_path
		elseif type(info)=="string" then msgs[#msgs+1] = info end
	end
	return nil, t_concat(msgs, '') --no file to load found
end

local function require(modname)
	assert(type(modname)=="string", "Bad argument #1 to 'require' (string expected)")
	modname:gsub("/+$", "") --remove any forward slashes at end

	local result, chunk, full_path = search_all(modname)
	if result==nil then
		assert(type(chunk)=="function", chunk) --if error then chunk is actually error msg
		result = chunk(full_path)
		p_loaded[full_path] = result==nil and true or result
		return p_loaded[full_path]
	else return result end
end

function meta:__call(...) return require(...) end

return _M


--[[ Copyright 2023 Llamazing
  []
  [] This program is free software: you can redistribute it and/or modify it under the
  [] terms of the GNU General Public License as published by the Free Software Foundation,
  [] either version 3 of the License, or (at your option) any later version.
  []
  [] It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [] without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [] PURPOSE.  See the GNU General Public License for more details.
  []
  [] You should have received a copy of the GNU General Public License along with this
  [] program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
